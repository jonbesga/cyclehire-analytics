from cyclehire.routes import app


def start_server():
    app.run(debug=True, port=5010)
