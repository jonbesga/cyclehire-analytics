import datetime

import requests
import untangle


def get_raw_data():
    url = 'https://tfl.gov.uk/tfl/syndication/feeds/cycle-hire/livecyclehireupdates.xml'
    r = requests.get(url)
    return r.text


previous_last_update = 0
total_data = []


def get_cycles_data(force_return=False):
    global previous_last_update
    global data
    obj = untangle.parse(get_raw_data())
    last_update = int(obj.stations['lastUpdate'])
    if last_update > previous_last_update:
        previous_last_update = last_update
        date = datetime.datetime.fromtimestamp(int(last_update / 1000)).strftime('%Y-%m-%d %H:%M')
        data = {'last_update': date, 'stations': []}
        for station in obj.stations.station:
            station_data = {}
            for attribute in station.children:
                if attribute._name in ['name', 'nbBikes', 'nbEmptyDocks', 'nbDocks', 'lat', 'long']:
                    station_data[attribute._name] = attribute.cdata
            data['stations'].append(station_data)
        total_data.append(data)
        return total_data[-1]
    else:
        if force_return:
            return total_data[-1]
        else:
            return None
