import json

from flask import Flask, render_template, request

from cyclehire.api import get_cycles_data

app = Flask(__name__)


@app.route("/")
def index():
    data = get_cycles_data(force_return=True)
    return render_template('index.html', data=data)


@app.route("/update")
def retrieve_data():
    force_return = bool(request.args.get('force_return'))
    data = get_cycles_data(force_return=force_return)
    return json.dumps(data)

@app.route("/map")
def map():
    return render_template('map.html')