from setuptools import setup, find_packages

setup(
    name='cyclehire-analytics',
    version='1.0.0',
    author='Jon Ander Besga',
    author_email='jon@wearejavit.com',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Programming Language :: Python :: 3.6',
    ],
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    entry_points={  # Optional
        'console_scripts': [
            'cyclehire=cyclehire.__main__:start_server'
        ],
    },
)
